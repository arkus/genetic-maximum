package helpers

import debugPrint
import models.Chromosome
import java.util.Random

object CrossoverHelper {
    private val random = Random()

    fun singlePointCrossover(pair: Pair<Chromosome, Chromosome>): Pair<Chromosome, Chromosome> {
        val point = random.nextInt(pair.first.data.size - 1) + 1
        val values1 = pair.first.data.take(point).toMutableList()
        values1.addAll(pair.second.data.drop(point))
        val values2 = pair.second.data.take(point).toMutableList()
        values2.addAll(pair.first.data.drop(point))

        val result = Chromosome(values1) to Chromosome(values2)
        debugPrint("[Crossover] Crossover with parents $pair -> $result (point: $point)")
        return result
    }

    fun twoPointCrossover(pair: Pair<Chromosome, Chromosome>): Pair<Chromosome, Chromosome> {
        return singlePointCrossover(singlePointCrossover(pair))
    }
}