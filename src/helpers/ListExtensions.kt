package helpers

fun List<Double>.sumUpTo(n: Int): Double {
    return this.take(n + 1).sum()
}