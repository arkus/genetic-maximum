package helpers

import debugPrint
import models.Chromosome
import mutationProbabilityThreshold
import java.util.Random

object MutateHelper {
    private val random = Random()

    fun mutate(chromosome: Chromosome): Chromosome {
        val values = chromosome.data.toMutableList()
        for (i in 0 until values.size) {
            if (random.nextDouble() < mutationProbabilityThreshold) {
                val before = values[i]
                val after = negate(before)
                debugPrint("[Mutate] Mutating bit on index $i of $chromosome ($before -> $after)")
                values[i] = after
            }
        }

        val mutated = Chromosome(values)

        if (chromosome.data != values) {
            debugPrint("[Mutate] Result: $chromosome -> $mutated")
        }

        return mutated
    }

    private fun negate(value: Int): Int {
        return if (value == 0) {
            1
        } else {
            0
        }
    }
}