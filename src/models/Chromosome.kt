package models

data class Chromosome(val data: List<Int>) {
    fun value(): Int {
        return data.reversed().reduceIndexed { index, acc, value ->
            acc + Math.pow((2 * value).toDouble(), index.toDouble()).toInt()
        }
    }

    override fun toString(): String {
        return "${this.data} (${this.value()})"
    }
}