import helpers.CrossoverHelper
import helpers.MutateHelper
import helpers.sumUpTo
import models.Chromosome
import java.util.*

const val populationSize = 50
const val iterationsCount = 50
const val chromosomeSize = 14
const val crossoverThreshold = 0.6
const val mutationProbabilityThreshold = 0.001
const val debug = false
const val twoPointsCrossover = true

val random = Random()

fun main(args: Array<String>) {
    var population: MutableList<Chromosome> = generateRandomPopulation()
    var best = 0
    var bestIteration = 0
    for (i in 0 until iterationsCount) {
        printStartOfIteration(i)
        val parents = selectParents(population)
        val children = crossover(parents)
        population = mutate(children)
        val fitnesses = population.map { it to fitness(it) }.sortedBy { it.second }.reversed()
        val bestChromosome = fitnesses.first().first
        val bestChromosomeValue = bestChromosome.value()
        if (best < bestChromosomeValue) {
            println("[Main] New best $bestChromosome at iteration no. ${i + 1}")
            best = bestChromosomeValue
            bestIteration = i
        }
        println("[Main] Best chromosome is $bestChromosome (f(x) = ${fitnesses.first().second})")
        println("[Main] Distance to all time best: ${best - bestChromosomeValue}")
    }
    println("[Main] All time best value is $best in iteration no. ${bestIteration + 1}")
    printEnd()
}

fun mutate(children: List<Chromosome>): MutableList<Chromosome> {
    val mutable = children.toMutableList()
    for (i in 0 until children.size) {
        mutable[i] = MutateHelper.mutate(children[i])
    }
    return mutable
}

fun crossover(parents: List<Chromosome>): List<Chromosome> {
    val children = mutableListOf<Chromosome>()
    val shuffled = parents.shuffled()
    val firstHalf = shuffled.take(populationSize / 2)
    val secondHalf = shuffled.takeLast(populationSize / 2)
    val pairs = firstHalf.mapIndexed { index, chromosome -> chromosome to secondHalf[index] }
    pairs.forEach {
        if (random.nextDouble() < crossoverThreshold) {
            val results = if (twoPointsCrossover) {
                CrossoverHelper.twoPointCrossover(it)
            } else {
                CrossoverHelper.singlePointCrossover(it)
            }
            children.add(results.first)
            children.add(results.second)
        } else {
            children.add(it.first)
            children.add(it.second)
        }
    }
    while (children.size < populationSize) {
        children.add(parents[random.nextInt(parents.size)])
    }
    return children
}

private fun printEnd() {
    println("---------------------------")
    println("---------- END ------------")
    println("---------------------------")
}

private fun printStartOfIteration(i: Int) {
    println("---------------------------")
    println("---- Iteration no. ${String.format("%03d", i + 1)} ----")
    println("---------------------------")
}

fun generateRandomPopulation(): MutableList<Chromosome> {
    val population = mutableListOf<Chromosome>()
    for (i in 0 until populationSize) {
        val randomValues: List<Int> = randomBits(chromosomeSize)
        val chromosome = Chromosome(randomValues)
        debugPrint("[Generate] Generated $chromosome")
        population.add(chromosome)
    }
    return population
}

fun randomBits(size: Int): List<Int> {
    val bits = mutableListOf<Int>()
    for (i in 0 until size) {
        bits.add(random.nextInt(2))
    }
    return bits
}

fun selectParents(population: List<Chromosome>): List<Chromosome> {
    val parents = mutableListOf<Chromosome>()
    val sumOfValues = population.map { fitness(it) }.reduce { acc, i -> acc + i }
    val probabilities = population.map { probability(it, sumOfValues) }
    for (i in 0 until population.size) {
        val randomValue = random.nextDouble()
        val value = Math.min(randomValue, probabilities.sumUpTo(probabilities.size))

        probabilities.forEachIndexed { index, _ ->
            if (value > probabilities.sumUpTo(index - 1) && value <= probabilities.sumUpTo(index)) {
                debugPrint("[Parents] Chosen parent at index $index with probability ${probabilities[index]}: ${population[index]}")
                parents.add(population[index])
            }
        }

    }
    return parents
}

fun debugPrint(message: String) {
    if (debug) {
        println(message)
    }
}

fun probability(chromosome: Chromosome, sumOfValues: Long): Double {
    return fitness(chromosome) / sumOfValues.toDouble()
}

fun fitness(chromosome: Chromosome): Long {
    return f(chromosome.value())
}

fun f(x: Int): Long {
    return (Math.pow(x.toDouble(), 4.0) + 3 * Math.pow(x.toDouble(), 3.0) - 13 * Math.pow(x.toDouble(), 2.0) + 32 * x.toDouble() + 174).toLong()
//    return (Math.pow(x.toDouble(), 3.0) + 2 * Math.pow(x.toDouble(), 2.0) + 12).toLong()
}
